# Banker (BigaBid exercise)

This is the Banker microservice. The Banker keep a balance per campaign, tell the service participating in the
auction if the campaign he’s looking to bid for has enough money to participate in an auction,
and reduce money in case a bid happens.

## Installation

Do you need install redis on your enviroment.
you can find the download here:
https://redis.io/download

### Install the project

```bash
git clone https://gitlab.com/omersh120/bigabidex.git
```

```bash
cd bigabidex
```

init redis with values in first use:
```bash
npm run initRedis
```

```bash
npm start
```

### Run API tests

for test:
```bash
npm test
```

## License
Developed by Omer Shalom September 2019 for BigaBid