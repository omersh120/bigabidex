const dal = require('../dal');
const hashMap = require('hashmap');
const transction = require('../dataModels/transaction');

let campaigns = new hashMap();
let transctions = [];

async function checkAuction(campaignId, cost) {
    let currBudget = await getBudget(campaignId);

    if (currBudget != null && currBudget >= cost) {
        createNewTransction(campaignId, cost);
        updateCampaign(campaignId, currBudget - cost);
        return true;
    }

    return false;
}

async function cancelTransaction(transactionId, campaignId) {
    try {
        let currTransaction = await getTransaction(transactionId);

        if (currTransaction != null) {
            if (currTransaction.isCancel)
                return true;

            currTransaction.isCancel = true;
            currBudget = await getBudget(campaignId);
            if (currBudget != null) {
                updateCampaign(campaignId, currBudget + currTransaction.paid);
                return true;
            }
        }

        return false;
    } catch (err) {
        console.log('Error at cancelTransaction: ' + err);
        return false;
    }

}

function syncRedis(time) {
    dal.updateCampaigns(campaigns);
    dal.updateTransactions(transctions);

    setTimeout(syncRedis, time, time);
}

function createNewTransction(campaignId, cost) {
    currTransaction = transction(campaignId, cost);
    transctions.push(currTransaction);
}

function updateCampaign(campaignId, newBudgget) {
    campaigns.delete(campaignId);
    campaigns.set(campaignId, newBudgget);
}

async function getTransaction(id) {
    let currTran = transctions.find(x => x.id === id);
    if (currTran == null) {
        try {
            let result = await dal.getTransaction(id);
            result.id = id;
            transctions.push(result);
            return result;
        } catch (err) {
            console.log('Error at getTransaction: ' + err);
            return null;
        }
    }
    return currTran;
}

async function getBudget(id) {
    let currCampaign = campaigns.get(id);
    if (currCampaign == null || isNaN(currCampaign)) {
        try {
            let result = await dal.getBudget(id);
            if (isNaN(result)) return null;

            campaigns.set(id, result);
            return result;
        } catch (err) {
            console.log('Error at getBudget: ' + err);
            return null;
        }
    }

    return currCampaign;
}

module.exports.syncRedis = syncRedis;
module.exports.checkAuction = checkAuction;
module.exports.cancelTransaction = cancelTransaction;