const redisClient = require('./index').redisClient;
const { campaign1, campaign2, campaign3 } = require('../dataModels/exampleCampaigns');
const { transaction1, transaction2, transaction3 } = require('../dataModels/expamleTransactions');

function insertDataToRedisInFirstUse() {
    console.log('Start insert examples data to redis');

    insertCampaignsExample();
    insertTransactionsExample();

    console.log('Finish to insert examples data');
}

function insertCampaignsExample() {
    redisClient.set(campaign1.id, campaign1.currBudget);
    redisClient.set(campaign2.id, campaign2.currBudget);
    redisClient.set(campaign3.id, campaign3.currBudget);
}

function insertTransactionsExample() {
    redisClient.set(transaction1.id, JSON.stringify(transaction1));
    redisClient.set(transaction2.id, JSON.stringify(transaction2));
    redisClient.set(transaction3.id, JSON.stringify(transaction3));
}

insertDataToRedisInFirstUse();