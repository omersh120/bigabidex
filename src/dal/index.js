const asyncRedis = require("async-redis");

const redisClient = asyncRedis.createClient();

redisClient.on('connect', function() {
    console.log('Redis Connection Successfull');
});

redisClient.on('error', (err) => {
    console.log("Redis Error " + err)
});

async function getBudget(campaignId) {
    try {
        let currBudget = await redisClient.get(campaignId);
        currBudget = parseFloat(currBudget);
        console.log('GET currBudget ->' + currBudget);
        return currBudget;
    } catch (err) {
        console.log('Error at getCurrBudget: ' + err);
        return null;
    }
}

async function getTransaction(id) {
    try {
        let currTransaction = JSON.parse(await redisClient.get(id));
        return currTransaction;
    } catch (err) {
        console.log('Error at currTransaction: ' + err);
        return null;
    }
}

function updateCampaigns(campaigns) {
    try {
        campaigns.forEach((value, key) => {
            redisClient.mset(key, value);
        });
    } catch (err) {
        console.log('Error at updateCampaigns: ' + err);
    }
}

function updateTransactions(transactions) {
    try {
        transactions.forEach((value) => {
            redisClient.mset(value.id, JSON.stringify(value));
        });
    } catch (err) {
        console.log('Error at updateCampaigns: ' + err);
    }
}

module.exports = {
    getBudget: getBudget,
    redisClient: redisClient,
    getTransaction: getTransaction,
    updateCampaigns: updateCampaigns,
    updateTransactions: updateTransactions
}