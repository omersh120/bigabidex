const uuid = require('uuid/v4');

function transction(campaignId, paid) {
    return {
        id: uuid(),
        campaignId: campaignId,
        paid: paid,
        isCancel: false
    }
}

module.exports = transction;