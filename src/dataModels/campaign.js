function campaign(id, currBudget) {
    return {
        id: id,
        currBudget: currBudget
    }
}

module.exports = campaign;