const transaction = require('./transaction');
const exampleCampaigns = require('./exampleCampaigns');


let transaction1 = transaction(exampleCampaigns.campaign1.id, 1);
transaction1.id = 11;

let transaction2 = transaction(exampleCampaigns.campaign2.id, 10000);
transaction2.id = 22;

let transaction3 = transaction(exampleCampaigns.campaign3.id, 100);
transaction3.id = 33;

module.exports.transaction1 = transaction1;
module.exports.transaction2 = transaction2;
module.exports.transaction3 = transaction3;