const express = require('express');
const bl = require('../bl');
const httpStatus = require('http-status-codes');

const router = express.Router();

router.post('/checkAuction', function(req, res, next) {
    let campaignId = req.body.campaignId;
    let cost = req.body.cost;

    if (cost != null && cost >= 0 && campaignId != null) {
        bl.checkAuction(campaignId, cost).then(result => {
            res.status(httpStatus.OK).send(result);
        });
    } else {
        res.status(httpStatus.BAD_REQUEST).send(false);
    }
});

router.put('/cancelAuction', function(req, res) {
    let transactionId = req.body.transactionId;
    let campaignId = req.body.campaignId;

    if (transactionId != null && campaignId != null) {
        bl.cancelTransaction(transactionId, campaignId).then((result) => {
            if (result === true) {
                res.status(httpStatus.OK).send();
            }

            res.status(httpStatus.INTERNAL_SERVER_ERROR).send();
        })
    } else {
        res.status(httpStatus.BAD_REQUEST).send();
    }
});


module.exports = router;