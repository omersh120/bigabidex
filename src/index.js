const express = require('express');
const api = require('./api');
const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const bl = require('./bl');

const app = express();
const appPort = 3001;
const updateRedisTimeMs = 1000 * 60 * 1;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger('dev'));

app.use('/api', api);

let server = app.listen(appPort, () => {
    console.log(`Banker is listening on port ${appPort}!`);
    bl.syncRedis(updateRedisTimeMs);
});

module.exports = server;