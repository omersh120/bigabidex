const assert = require("assert");
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../src");
const httpStatus = require('http-status-codes');
const exampleCampaign = require('../../src/dataModels/exampleCampaigns').campaign1;
const exampleTransction = require('../../src/dataModels/expamleTransactions').transaction1;

const expect = chai.expect;
const should = chai.should();

chai.use(chaiHttp);

describe("BankerAPI", function() {
    describe("Check auction", function() {
        it("should return true, cost is smaller than budget", done => {
            let currCampign = {
                "campaignId": exampleCampaign.id,
                "cost": 0.1
            }

            chai.request(server)
                .post("/api/checkAuction")
                .send(currCampign)
                .end((err, res) => {
                    res.should.have.status(httpStatus.OK);
                    expect(res.body).to.be.true;
                    console.log("Response Body:", res.body);
                    done();
                })
        })

        it("should return false, cost is bigger than budget", done => {
            let currCampign = {
                "campaignId": exampleCampaign.id,
                "cost": exampleCampaign.currBudget + 10
            }

            chai.request(server)
                .post("/api/checkAuction")
                .send(currCampign)
                .end((err, res) => {
                    res.should.have.status(httpStatus.OK);
                    expect(res.body).to.be.false;
                    console.log("Response Body:", res.body);
                    done();
                })
        })

        it("should return false, campaignId not exist in the system", done => {
            let currCampign = {
                "campaignId": 'notExistId',
                "cost": 10
            }

            chai.request(server)
                .post("/api/checkAuction")
                .send(currCampign)
                .end((err, res) => {
                    res.should.have.status(httpStatus.OK);
                    expect(res.body).to.be.false;
                    console.log("Response Body:", res.body);
                    done();
                })
        })

        it("should return false, campaignId not exist in the request", done => {
            let currCampign = {
                "cost": 10
            }

            chai.request(server)
                .post("/api/checkAuction")
                .send(currCampign)
                .end((err, res) => {
                    res.should.have.status(httpStatus.BAD_REQUEST);
                    expect(res.body).to.be.false;
                    console.log("Response Body:", res.body);
                    done();
                })
        })

        it("should return false, cost not exist in the request", done => {
            let currCampign = {
                "campaignId": exampleCampaign.id
            }

            chai.request(server)
                .post("/api/checkAuction")
                .send(currCampign)
                .end((err, res) => {
                    res.should.have.status(httpStatus.BAD_REQUEST);
                    expect(res.body).to.be.false;
                    console.log("Response Body:", res.body);
                    done();
                })
        })

        it("should return false, cost is negative", done => {
            let currCampign = {
                "campaignId": exampleCampaign.id,
                "cost": -1
            }

            chai.request(server)
                .post("/api/checkAuction")
                .send(currCampign)
                .end((err, res) => {
                    res.should.have.status(httpStatus.BAD_REQUEST);
                    expect(res.body).to.be.false;
                    console.log("Response Body:", res.body);
                    done();
                })
        })
    })

    describe("Cancel auction", function() {
        it("should return Status.Ok, params ok", done => {
            let input = {
                "campaignId": exampleTransction.campaignId,
                "transactionId": exampleTransction.id
            }

            chai.request(server)
                .put("/api/cancelAuction")
                .send(input)
                .end((err, res) => {
                    res.should.have.status(httpStatus.OK);
                    console.log("Response Body:", res.body);
                    done();
                })
        })

        it("should return Status.Bad_request, campaignId is missing", done => {
            let input = {
                "transactionId": exampleTransction.id
            }

            chai.request(server)
                .put("/api/cancelAuction")
                .send(input)
                .end((err, res) => {
                    res.should.have.status(httpStatus.BAD_REQUEST);
                    console.log("Response Body:", res.body);
                    done();
                })
        })

        it("should return Status.Bad_request, transcionId is missing", done => {
            let input = {
                "campaignId": exampleTransction.campaignId
            }

            chai.request(server)
                .put("/api/cancelAuction")
                .send(input)
                .end((err, res) => {
                    res.should.have.status(httpStatus.BAD_REQUEST);
                    console.log("Response Body:", res.body);
                    done();
                })
        })

        it("should return Status.INTERNAL_SERVER_ERROR, transcionId is no exist", done => {
            let input = {
                "campaignId": exampleTransction.campaignId,
                "transactionId": 'NoExistId'
            }

            chai.request(server)
                .put("/api/cancelAuction")
                .send(input)
                .end((err, res) => {
                    res.should.have.status(httpStatus.INTERNAL_SERVER_ERROR);
                    console.log("Response Body:", res.body);
                    done();
                })
        })

        it("should return Status.INTERNAL_SERVER_ERROR, campaignId is no exist", done => {
            let input = {
                "campaignId": 'NoExistId',
                "transactionId": exampleTransction.campaignId
            }

            chai.request(server)
                .put("/api/cancelAuction")
                .send(input)
                .end((err, res) => {
                    res.should.have.status(httpStatus.INTERNAL_SERVER_ERROR);
                    console.log("Response Body:", res.body);
                    done();
                })
        })

    })
})